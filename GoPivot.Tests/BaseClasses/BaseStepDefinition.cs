﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using GoPivot.Tests.ComponentHelper;
using GoPivot.Tests.Configuration;
using GoPivot.Tests.CustomException;
using GoPivot.Tests.Settings;
using log4net.Repository.Hierarchy;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace GoPivot.Tests.BaseClasses
{
    [Binding]
    public class BaseStepDefinition
    {
        public static IWebDriver driver;
        public static ExtentReports extent;
        public static ExtentHtmlReporter htmlReporter;
        public static ExtentTest extentTest;

        [BeforeTestRun]
        public static void Init()
        {
            ObjectRepository.Config = new AppConfigReader();

            switch (ObjectRepository.Config.GetBrowser())
            {
                case BrowserType.Firefox:
                    driver = GetFirefoxDriver();

                    break;

                case BrowserType.Chrome:
                    driver = GetChromeDriver();
                    break;

                case BrowserType.IExplorer:
                    driver = GetIEDriver();
                    break;

                default:
                    throw new NoSutiableDriverFound("Driver Not Found : " + ObjectRepository.Config.GetBrowser().ToString());
            }
            driver.Manage()
                .Timeouts().PageLoad = TimeSpan.FromSeconds(ObjectRepository.Config.GetPageLoadTimeOut());
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ObjectRepository.Config.GetElementLoadTimeOut());
            driver.Manage().Window.Maximize();
            ObjectRepository.Driver = driver;
            driver.Navigate().GoToUrl(ObjectRepository.Config.GetWebsite());

        }

        private static FirefoxDriver GetFirefoxDriver()
        {
            FirefoxOptions options = new FirefoxOptions();
            FirefoxDriver driver = new FirefoxDriver(options);
            return driver;
        }

        private static ChromeDriver GetChromeDriver()
        {
            ChromeDriver driver = new ChromeDriver(GetChromeOptions());
            return driver;
        }

        private static InternetExplorerDriver GetIEDriver()
        {
            InternetExplorerDriver driver = new InternetExplorerDriver(GetIEOptions());
            return driver;
        }

        private static FirefoxProfile GetFirefoxptions()
        {
            FirefoxProfile profile = new FirefoxProfile();
            FirefoxProfileManager manager = new FirefoxProfileManager();
            return profile;
        }

        private static FirefoxOptions GetOptions()
        {
            FirefoxProfileManager manager = new FirefoxProfileManager();

            FirefoxOptions options = new FirefoxOptions()
            {
                Profile = manager.GetProfile("default"),
                AcceptInsecureCertificates = true,

            };
            return options;
        }
        private static ChromeOptions GetChromeOptions()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArgument("start-maximized");
            return option;
        }

        private static InternetExplorerOptions GetIEOptions()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.EnsureCleanSession = true;
            options.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Bottom;
            return options;
        }

        [BeforeTestRun]
        [OneTimeSetUp]
        public static void SetupReporting()
        {

            var pth = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            var actualPath = pth.Substring(0, pth.LastIndexOf("bin"));
            var path = new Uri(actualPath).LocalPath;

            String ReportsPath = Path.Combine(path, "Reports", "GoPivot_TestCases.html");

            htmlReporter = new ExtentHtmlReporter(ReportsPath);

            htmlReporter.Configuration().Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Standard;

            htmlReporter.Configuration().DocumentTitle = "GoPivot";

            htmlReporter.Configuration().ReportName = "GoPivot Test Report";


            extent = new ExtentReports();

            extent.AttachReporter(htmlReporter);
        }

        [AfterTestRun]
        public static void End()
        {
            if (driver != null)
            {
                driver.Close();
                driver.Quit();
                 
            }
        }

        [AfterTestRun]
        [OneTimeTearDown]
        public static void GenerateReport()
        {
            extent.Flush();

        }

    }
}
