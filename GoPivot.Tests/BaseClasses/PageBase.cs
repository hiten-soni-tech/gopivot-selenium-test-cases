﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using GoPivot.Tests.ComponentHelper;

namespace GoPivot.Tests.BaseClasses
{
    public class PageBase
    {
        private IWebDriver driver;
  


        [FindsBy(How = How.LinkText, Using = "Log Out")]
        public IWebElement LogOut;

        public PageBase(IWebDriver _driver)
        {
            PageFactory.InitElements(_driver,this);
            this.driver = _driver;
        }
         
        public string Title
        {
            get { return driver.Title; }
        }
    }
}
