﻿using GoPivot.Tests.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;


namespace GoPivot.Tests.PageObject
{
    public class AdminPage : PageBase
    {




        private IWebDriver driver;
        [FindsBy(How = How.LinkText, Using = "Clients")]
        public IWebElement Clients;

        [FindsBy(How = How.LinkText, Using = "Edit")]
        public IWebElement EditClient;

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div/div/div/div/ul/li")]
        public IList<IWebElement> ProgramTabs;

        public IWebElement AddChallengeButton;


        public AdminPage(IWebDriver _driver) : base(_driver)
        {
            PageFactory.InitElements(_driver, this);
            this.driver = _driver;
        }






        private WebDriverWait GetWebDriverWait(IWebDriver driver, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout)
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        public IWebElement GetProgramTab(string tabName)
        {
            try
            {
                string path = string.Format("//ul[@class = 'nav nav-tabs']/li/a[contains(.,'{0}')]", tabName);
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                return wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
            }
            catch
            {
                return null;
            }

        }

        public IWebElement GetChallengeCategory(string category)
        {
            try
            {
                string path = string.Format("//div[@class='col-xs-12']/button[contains(.,'{0}')]", category);
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                return wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(path)));
            }
            catch
            {
                return null;
            }
        }

        public IWebElement GetClient(string client)
        {
            try
            {
                return driver.FindElement(By.LinkText(client));
            }
            catch
            {
                // element not found
                return null;
            }
        }

        public IWebElement GetProgram(string program)
        {
            try
            {
                return driver.FindElement(By.XPath(string.Format("//h3[contains(text(),'{0}')]", program)));
            }
            catch
            {
                return null;
            }
        }

        public IWebElement GetChallengeType(string challengeType)
        {
            try
            {
                return driver.FindElement(By.XPath(string.Format("//div[contains(@class,'challengeTemplate ng-scope')]/div[@class='templateName ng-binding' and contains(text(),'{0}')]", challengeType)));
            }
            catch
            {
                return null;
            }
        }

        public void GetAddChallengeButton()
        {

            try
            {
                string path = "/html/body/div/div/div/div/div[2]/div/div/div/div/div/div/div[6]/ng-include/div/div[1]/div[3]/button";
                WebDriverWait wait = GetWebDriverWait(driver, TimeSpan.FromSeconds(60));
                AddChallengeButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
            }
            catch
            {
                AddChallengeButton = null;
            }
            //bool staleElement = true;
            //int i = 0;
            //while (staleElement && i < 5)
            //{
            //    try
            //    {
            //        AddChallengeButton = driver.FindElement(By.XPath("/html/body/div/div/div/div/div[2]/div/div/div/div/div/div/div[6]/ng-include/div/div[1]/div[3]/button"));
            //        staleElement = false;
            //    }
            //    catch (StaleElementReferenceException e)
            //    {
            //        i++;
            //        AddChallengeButton = null;
            //    }
            //}
            //if (AddChallengeButton != null && !AddChallengeButton.Displayed)
            //{
            //    AddChallengeButton = null;
            //}
        }

    }
}
