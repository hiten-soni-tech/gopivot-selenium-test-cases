﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using GoPivot.Tests.BaseClasses;
using GoPivot.Tests.ComponentHelper;
using GoPivot.Tests.Settings;

namespace GoPivot.Tests.PageObject
{
    public class LoginPage : PageBase
    {
        private IWebDriver driver;


        #region WebElement
        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement LoginTextBox;
        //private IWebElement LoginTextBox => driver.FindElement(By.Id("Bugzilla_login"));

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement PassTextBox;
        //private IWebElement PassTextBox => driver.FindElement(By.Id("Bugzilla_password"));

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/form/div[4]/button")]
        [CacheLookup]
        public IWebElement LoginButton;
        //private IWebElement LoginButton => driver.FindElement(By.Id ("log_in"));

        

        #endregion

        public LoginPage(IWebDriver _driver) : base(_driver)
        {
            PageFactory.InitElements(_driver, this);
            this.driver = _driver;
           
        }
 

      
    }
}
