﻿using GoPivot.Tests.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;
using GoPivot.Tests.ComponentHelper;
using System.Threading;

namespace GoPivot.Tests.PageObject
{
    public class AddNewChallengePage : PageBase
    {


        private IWebDriver driver;
        [FindsBy(How = How.Id, Using = "challengeName")]
        public IWebElement ChallengeName;

        [FindsBy(How = How.Id, Using = "input_1")]
        public IWebElement ChallengeStartDate;

        [FindsBy(How = How.Id, Using = "input_5")]
        public IWebElement ChallengeEndDate;

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input")]
        public IWebElement GoalValue;

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select")]
        public IWebElement Timeline;

        [FindsBy(How = How.Id, Using = "goal")]
        public IWebElement Description;

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input[1]")]
        public IWebElement CustomChallenge_PerformAction;

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/input[2]")]
        public IWebElement CustomChallenge_GoalValue;


        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div/div[2]/div/div[2]/div/form/div/div[2]/button[2]")]
        public IWebElement Step1_NextButton;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder = 'Search participants']")]
        public IWebElement Step2_SearchParticipantText;

        [FindsBy(How = How.XPath, Using = "//div[@class = 'scrollableList']/div[@class='ng-isolate-scope']/div[@class='ng-scope']")]
        public IList<IWebElement> Step2_ParticipantList;

        [FindsBy(How = How.XPath, Using = "//button[contains(.,'Create')]")]
        public IWebElement Step2_CreateButton;

        [FindsBy(How = How.XPath, Using = "//button[contains(.,'Everyone')]")]
        public IWebElement Step2_EveryOne;


        public AddNewChallengePage(IWebDriver _driver) : base(_driver)
        {
            PageFactory.InitElements(_driver, this);
            this.driver = _driver;
        }

        public bool FillStep1(string challengeName, string startDate, string endDate, string goalValue, string timeline, string description, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeName.SendKeys(challengeName);
                ChallengeStartDate.SendKeys(startDate);
                ChallengeEndDate.SendKeys(endDate);
                GoalValue.SendKeys(goalValue);
                ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select"), timeline);
                Description.SendKeys(description);
                success = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }

        public bool FillStepForJumpStart(string challengeName, string startDate, string endDate, string description, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeName.SendKeys(challengeName);
                ChallengeStartDate.SendKeys(startDate);
                ChallengeEndDate.SendKeys(endDate);
                Description.SendKeys(description);
                success = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }


        public bool FillStepForCustom(string challengename, string startdate, string enddate, string customchallengeactiontext, string customchallengegoal, string customchallengeunit, string customchallengetime, string goalText, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                ChallengeName.Clear();
                ChallengeName.SendKeys(challengename);
                ChallengeStartDate.SendKeys(startdate);
                ChallengeEndDate.SendKeys(enddate);
                CustomChallenge_PerformAction.SendKeys(customchallengeactiontext);
                CustomChallenge_GoalValue.SendKeys(customchallengegoal);
                
                ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[1]"), customchallengeunit);
                var customChallengeTimeBox = ComboBoxHelper.GetAllItem(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[2]"));
                var selectedValue = customChallengeTimeBox.FirstOrDefault(p => p.Contains(customchallengetime));
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    ComboBoxHelper.SelectElementByValue(By.XPath("/html/body/div/div/div/div/div[2]/div/div[2]/div/form/ng-include[2]/div/div[4]/select[2]"), selectedValue);
                    Description.SendKeys(goalText);
                    success = true;
                }
                else
                {
                    errorMessage = "No Custom Challenge Time value found";
                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return success;

        }

        public bool AddChallengers(string challengername, out string errorMessage)
        {
            bool success = false;
            errorMessage = string.Empty;
            try
            {
                Step2_SearchParticipantText.SendKeys(challengername);
                Thread.Sleep(2000);
                string xPath = string.Format("//div[@class = 'user ng-scope' and contains(.,'{0}')]", challengername);
                var participantElements = driver.FindElements(By.XPath(xPath));
                int participantsAdded = 0;
                foreach (var element in participantElements)
                {
                    element.Click();
                    participantsAdded++;
                }
                if (participantsAdded > 0)
                {
                    success = true;
                }
                else
                {
                    errorMessage = "No Participants added";
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return success;
        }
    }
}
