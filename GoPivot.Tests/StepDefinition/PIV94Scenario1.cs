﻿using GoPivot.Tests.BaseClasses;
using GoPivot.Tests.ComponentHelper;
using GoPivot.Tests.PageObject;
using GoPivot.Tests.Settings;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;


namespace GoPivot.Tests.StepDefinition
{
    [Binding, Scope(Feature = "PIV-94 Scenario 1")]
    public class PIV94Scenario1 : BaseStepDefinition
    {


        private LoginPage loginPage;

        [Given(@"I login to admin view (.*), (.*) and click on login button")]
        public void GivenILoginToAdminViewAndClickOnLoginButton(string username, string password)
        {
            extentTest = extent.CreateTest(ScenarioContext.Current.ScenarioInfo.Title);
            loginPage = new LoginPage(driver);
            extentTest.Info("Login Page loaded successfully");


            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            loginPage.LoginButton.Click();

            extentTest.Info(string.Format("Administrator {0} Logged in successfully", username));


            extentTest.Info("Admin View Loaded");

           
           
        }

        [When(@"Selects (.*) from the list")]
        public void WhenSelectsFromTheList(string client)
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            var clientElement = adminPage.GetClient(client);
            if (clientElement != null && clientElement.Displayed)
            {
                extentTest.Info(string.Format("Client {0} selected", client));
                clientElement.Click();
            }
            else
            {
                extentTest.Error(string.Format("Client {0} not found", client));
                Assert.Fail(string.Format("Client {0} not found", client));
            }
        }

        [When(@"Clicks (.*) from Program List")]
        public void WhenClicksFromProgramList(string program)
        {
            var adminPage = new AdminPage(driver);
            var programElement = adminPage.GetProgram(program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                extentTest.Info(string.Format("Program {0} selected", program));
            }
            else
            {
                extentTest.Error(string.Format("Program {0} not found", program));
            }
        }

        [When(@"Admin Selects Challenges Tab")]
        public void WhenAdminSelectsChallengesTab()
        {
            Thread.Sleep(1000);
            var adminPage = new AdminPage(driver);
            var challengeTab = adminPage.GetProgramTab("Challenges");
            if (challengeTab == null)
            {
                extentTest.Error("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                extentTest.Info("Challenge Tab loaded");
                challengeTab.Click();
            }

        }

        [When(@"Clicks on New Challenge")]
        public void WhenClicksOnNewChallenge()
        {
            Thread.Sleep(5000);
            AdminPage adminPage = new AdminPage(driver);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                extentTest.Info("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                extentTest.Error("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }
        }

        [When(@"Clicks on Wellness Type of Challenge")]
        public void WhenClicksOnWellnessTypeOfChallenge()
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeCategory = adminPage.GetChallengeCategory("Wellness");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                extentTest.Info("Wellness Challenge type selected");
            }
            else
            {
                extentTest.Error("Wellness Challenge Type not available");
                Assert.Fail("Wellness Challenge Type not available");
            }

        }

        [When(@"Selects (.*) from the challenge type list")]
        public void WhenSelectsFromTheChallengeTypeList(string wellnesschallengetype)
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeType = adminPage.GetChallengeType(wellnesschallengetype);
            if (challengeType != null)
            {
                challengeType.Click();
                extentTest.Info(string.Format("{0} Challenge Type selected", wellnesschallengetype));
            }
            else
            {
                string errorMessage = string.Format("{0} Challenge type not found", wellnesschallengetype);
                extentTest.Error(errorMessage);
                Assert.Fail(errorMessage);
            }

        }


        [When(@"i fill (.*), (.*), (.*), (.*), (.*) and (.*)")]
        public void WhenIFillAnd(string challengename, string startdate, string enddate, string goalvalue, string timeline, string description)
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            bool success = addNewChallengePage.FillStep1(challengename, startdate, enddate, goalvalue, timeline, description, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                extentTest.Info("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                extent.Equals(errorMessage);
                Assert.Fail(errorMessage);
            }
        }


        [When(@"Searches for Participant (.*) and add into right section")]
        public void WhenSearchesForParticipantAndAddIntoRightSection(string challengername)
        {
            AddNewChallengePage addChallengerPage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            bool success = addChallengerPage.AddChallengers(challengername, out errorMessage);
            if (success)
            {
                extentTest.Info(string.Format("Participant {0} added to challenge", challengername));
            }
            else
            {
                extentTest.Error(string.Format("Failed to add {0} challenger. Error: {1}", challengername, errorMessage));
                Assert.Fail(errorMessage);
            }

        }

        [When(@"User tries to save new challenge")]
        public void WhenUserTriesToSaveNewChallenge()
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            addNewChallengePage.Step2_CreateButton.Click();
            extentTest.Info("Create Challenge button clicked");
        }

        [Then(@"Client link should be visible")]
        public void ThenClientLinkShouldBeVisible()
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                extentTest.Info("Clients link clicked");
            }
            else
            {
                extentTest.Fail("Clients Link not found");
                Assert.Fail("Clients link not found");
            }
        }

        [Then(@"Client details page will be loaded")]
        public void ThenClientDetailsPageWillBeLoaded()
        {

            var adminPage = new AdminPage(driver);
            if (!adminPage.EditClient.Displayed)
            {
                extentTest.Fail("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                extentTest.Info("Client Details page loaded");
            }
        }

        [Then(@"Program tabs will be listed")]
        public void ThenProgramTabsWillBeListed()
        {
            var adminPage = new AdminPage(driver);
            if (adminPage.ProgramTabs.Count > 0)
            {
                extentTest.Info("Program Listing page loaded");

            }
            else
            {
                extentTest.Error("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }
        }

        [Then(@"Participants should be listed")]
        public void ThenParticipantsShouldBeListed()
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                extentTest.Info("Participants available to select");
            }
            else
            {
                extentTest.Error("No Participants available");
                Assert.Fail("No Participants available");
            }
        }

        [Then(@"New challenge (.*) should be listed in challenge list")]
        public void ThenNewChallengeShouldBeListedInChallengeList(string challengename)
        {
            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                string message = string.Format("Challenge {0} added successfully", challengename);
                extentTest.Pass(message);
                FeatureContext.Current["ChallengeCreated"] = true;
                Assert.IsTrue(true);
            }
            else
            {
                string errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                extentTest.Fail(errorMessage);
            }
        }

        [Given(@"User login to participant view using (.*),(.*) and click on login button")]
        public void Given_User_Logged_Into_Participant_View(string username, string password)
        {

            try
            {
                loginPage = new LoginPage(driver);
                extentTest.Info("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(username);
                loginPage.PassTextBox.SendKeys(password);
                loginPage.LoginButton.Click();

                IWebElement logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    extentTest.Info(string.Format("Participant {0} Logged in successfully", username));
                    extentTest.Info("Participant View Loaded");
                }
                else
                {
                    extentTest.Fail("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }
            }
            catch
            {
                extentTest.Fail("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view");
            }



        }

        [Given(@"Click on Challenges Tab")]
        public void Given_Click_On_Challenges_Tab()
        {
            try
            {
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    extentTest.Info("Challenge Tab Clicked");
                }
            }
            catch (Exception ex)
            {
                extentTest.Fail("Failed to click on challenge tab");
                Assert.Fail(string.Format("Failed to load Challenge Tab. Error {0}", ex.Message));

            }

        }

        [Then(@"Challenge List should be visible")]
        public void Then_Challenge_List_Should_Be_Visible()
        {
            try
            {
                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    extentTest.Info(string.Format("{0} Challenges available for participant", challengeList.Count));
                }
                else
                {
                    extentTest.Error("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }
            }
            catch (Exception ex)
            {
                extentTest.Error("Failed to load challenges");
                Assert.Fail("Failed to load any challenge. Error: {0}", ex.Message);
            }
        }

        [When(@"(.*) found from available section and clicked")]
        public void When_ChallengeName_Found_From_Available_Section_And_Clicked(string challengename)
        {
            try
            {
                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", challengename))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    extentTest.Info(string.Format("{0} challenge found", challengename));

                }

                else
                {
                    extentTest.Error(string.Format("{0} challenge not found", challengename));
                    Assert.Fail(string.Format("{0} challenge not found", challengename));
                }
            }
            catch (Exception ex)
            {
                extentTest.Error("Failed to load challenges");
                Assert.Fail("Failed to load any challenge. Error: {0}", ex.Message);
            }

        }

        [Then(@"Goal (.*) should be equal to challenge created")]
        public void Then_Goal_Should_Be_Equal_To_Challenge_Created(string goaldescription)
        {
            try
            {
                var goalTextElement = driver.FindElement(By.XPath("//div[@class='ng-scope']/h2[contains(.,'Goal')]/following-sibling::p[1]"));
                if (goalTextElement != null && goalTextElement.Displayed && goalTextElement.Text.ToLower().Contains(goaldescription.ToLower()))
                {
                    extentTest.Pass(string.Format("Goal {0} matching", goaldescription));


                }
                else
                {
                    extentTest.Error("Failed to load goal details");
                    Assert.Fail("Failed to validate goal. Error: Element not found or text not matching");

                }
            }
            catch (Exception ex)
            {
                extentTest.Error("Failed to load goal details");
                Assert.Fail("Failed to validate goal. Error: {0}", ex.Message);
            }
        }



        [Given(@"Healthy Weight Loss Challenge created successfully")]
        public void Given_Healthy_Weight_Loss_Challenge_Created_Successfully()
        {
            extentTest = extent.CreateTest(ScenarioContext.Current.ScenarioInfo.Title);
            if (FeatureContext.Current.ContainsKey("ChallengeCreated") && (bool)FeatureContext.Current["ChallengeCreated"])
            {
                Assert.IsTrue(true);
            }
            else
            {
                extentTest.Error("Challenge not created");
                Assert.Fail("Previous Test Case to create challenge failed");
            }
        }

        [Given(@"Administrator logged out of system")]
        public void Given_Administrator_Logged_Out_Of_System()
        {
            Thread.Sleep(1000);
            try
            {
                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        extentTest.Info("Administrator logged off successfully");
                    }
                    else
                    {
                        extentTest.Error("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    extentTest.Error("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }
            }
            catch (Exception ex)
            {
                extentTest.Error("Failed to logoff administrator");
                Assert.Fail("Failed to logout administrator. Error: {0}", ex.Message);
                // do nothing
            }

        }





        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }


    }
}
