﻿using GoPivot.Tests.BaseClasses;
using GoPivot.Tests.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports;

namespace GoPivot.Tests.StepDefinition
{
    [Binding]
    [Scope(Feature = "PIV-1704")]
    public class PIV1704Scenario : BaseStepDefinition
    {
        private LoginPage loginPage;
        private ExtentTest CreateChallengeTest;
        private ExtentTest ParticipantListTest;
        [Given(@"I login to admin view (.*), (.*) and click on login button")]
        public void GivenILoginToAdminViewAndClickOnLoginButton(string username, string password)
        {
            extentTest = extent.CreateTest("PIV-1704");
            CreateChallengeTest = extentTest.CreateNode("Create Future Custom One time Challenge");
            loginPage = new LoginPage(driver);
            CreateChallengeTest.Info("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            loginPage.LoginButton.Click();
            CreateChallengeTest.Info(string.Format("Administrator {0} Logged in successfully", username));
        }


        [When(@"Selects (.*) from the list")]
        public void WhenSelectsFromTheList(string client)
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            var clientElement = adminPage.GetClient(client);
            if (clientElement != null && clientElement.Displayed)
            {
                CreateChallengeTest.Info(string.Format("Client {0} selected", client));
                clientElement.Click();
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Client {0} not found", client));
                Assert.Fail(string.Format("Client {0} not found", client));
            }
        }
        [When(@"Clicks (.*) from Program List")]
        public void WhenClicksFromProgramList(string program)
        {
            var adminPage = new AdminPage(driver);
            var programElement = adminPage.GetProgram(program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                CreateChallengeTest.Info(string.Format("Program {0} selected", program));
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Program {0} not found", program));
            }
        }

        [When(@"Admin Selects Challenges Tab")]
        public void WhenAdminSelectsChallengesTab()
        {
            Thread.Sleep(1000);
            var adminPage = new AdminPage(driver);
            var challengeTab = adminPage.GetProgramTab("Challenges");
            if (challengeTab == null)
            {
                CreateChallengeTest.Error("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                CreateChallengeTest.Info("Challenge Tab loaded");
                challengeTab.Click();
            }

        }

        [When(@"Clicks on New Challenge")]
        public void WhenClicksOnNewChallenge()
        {
            Thread.Sleep(5000);
            AdminPage adminPage = new AdminPage(driver);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                CreateChallengeTest.Info("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                CreateChallengeTest.Error("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }
        }
        [When(@"Clicks on Custom Type of Challenge")]
        public void WhenClicksOnCustomTypeOfChallenge()
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeCategory = adminPage.GetChallengeCategory("Custom");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                CreateChallengeTest.Info("Custom Challenge type selected");
            }
            else
            {
                CreateChallengeTest.Error("Custom Challenge Type not available");
                Assert.Fail("Custom Challenge Type not available");
            }
        }

        [When(@"Selects first option from challenge types")]
        public void WhenSelectsFirstOptionFromChallengeTypes()
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeType = adminPage.GetChallengeType("Custom");
            if (challengeType != null)
            {
                challengeType.Click();
                CreateChallengeTest.Info("Custom Challenge selected");
            }
            else
            {
                string errorMessage = "Custom Challenge not found";
                CreateChallengeTest.Error(errorMessage);
                Assert.Fail(errorMessage);
            }
        }

        [When(@"i fill (.*),(.*),(.*),(.*),(.*),(.*),(.*) and (.*)")]
        public void WhenIFillCustomChallenge(string challengename, string startdate, string enddate, string customchallengeactiontext, string customchallengegoal, string customchallengeunit, string customchallengetime, string goalText)
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            string randomNumber = new Random().Next(1000).ToString();
            challengename = challengename.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
            challengename = challengename.Replace("{currentday}", DateTime.Now.ToShortDateString());
            challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
            challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());
            challengename = challengename.Replace("{randomnumber}", randomNumber);
            FeatureContext.Current.Add("ChallengeRandomNumber", randomNumber);
            startdate = startdate.Replace("{currentdate}", DateTime.Now.ToShortDateString());
            startdate = startdate.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
            startdate = startdate.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
            startdate = startdate.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());

            DateTime reference = DateTime.Now;
            DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
            DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
            DateTime lastDayNextMonth = firstDayPlusTwoMonths.AddDays(-1);
            DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);
            enddate = enddate.Replace("{endofnextmonth}", endOfLastDayNextMonth.ToShortDateString());

            bool success = addNewChallengePage.FillStepForCustom(challengename, startdate, enddate, customchallengeactiontext, customchallengegoal, customchallengeunit, customchallengetime, goalText, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                CreateChallengeTest.Info("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                extent.Equals(errorMessage);
                Assert.Fail(errorMessage);
            }
        }

        [When(@"Searches for Participant (.*) and add into right section")]
        public void WhenSearchesForParticipantAndAddIntoRightSection(string challengername)
        {
            Thread.Sleep(1000);
            AddNewChallengePage addChallengerPage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            bool success = addChallengerPage.AddChallengers(challengername, out errorMessage);
            if (success)
            {
                CreateChallengeTest.Info(string.Format("Participant {0} added to challenge", challengername));
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Failed to add {0} challenger. Error: {1}", challengername, errorMessage));
                Assert.Fail(errorMessage);
            }

        }


        [Then(@"Client link should be visible")]
        public void ThenClientLinkShouldBeVisible()
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                CreateChallengeTest.Info("Clients link clicked");
            }
            else
            {
                CreateChallengeTest.Fail("Clients Link not found");
                Assert.Fail("Clients link not found");
            }
        }

        [Then(@"Client details page will be loaded")]
        public void ThenClientDetailsPageWillBeLoaded()
        {

            var adminPage = new AdminPage(driver);
            if (!adminPage.EditClient.Displayed)
            {
                CreateChallengeTest.Fail("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                CreateChallengeTest.Info("Client Details page loaded");
            }
        }

        [Then(@"Program tabs will be listed")]
        public void ThenProgramTabsWillBeListed()
        {
            var adminPage = new AdminPage(driver);
            if (adminPage.ProgramTabs.Count > 0)
            {
                CreateChallengeTest.Info("Program Listing page loaded");

            }
            else
            {
                CreateChallengeTest.Error("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }
        }

        [Then(@"Participants should be listed")]
        public void ThenParticipantsShouldBeListed()
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                CreateChallengeTest.Info("Participants available to select");
            }
            else
            {
                CreateChallengeTest.Error("No Participants available");
                Assert.Fail("No Participants available");
            }
        }

        [When(@"Every participants added and saves the challenge")]
        public void WhenEveryParticipantsAddedAndSavesTheChallenge()
        {
            try
            {
                AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
                addNewChallengePage.Step2_EveryOne.Click();
                Thread.Sleep(1000);
                CreateChallengeTest.Info("Everyone as participants been selected");
                Thread.Sleep(1000);
                addNewChallengePage.Step2_CreateButton.Click();
                CreateChallengeTest.Info("Create Challenge button clicked");
            }
            catch (Exception ex)
            {
                CreateChallengeTest.Error("Error while selecting participants");
                Assert.Fail("Error while selecting participants. Error: {0}", ex.ToString());
            }

        }


        [Then(@"New challenge (.*) should be listed in challenge list")]
        public void ThenNewChallengeShouldBeListedInChallengeList(string challengename)
        {

            string randomNumber = FeatureContext.Current != null && FeatureContext.Current.ContainsKey("ChallengeRandomNumber") ? FeatureContext.Current["ChallengeRandomNumber"].ToString() : string.Empty;
            challengename = challengename.Replace("{previousday}", DateTime.Now.AddDays(-1).ToShortDateString());
            challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
            challengename = challengename.Replace("{currentday}", DateTime.Now.ToShortDateString());
            challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());

            challengename = challengename.Replace("{randomnumber}", randomNumber);
            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                string message = string.Format("Challenge {0} added successfully", challengename);
                CreateChallengeTest.Pass(message);
                FeatureContext.Current["ChallengeCreated"] = true;
                Assert.IsTrue(true);
            }
            else
            {
                string errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                CreateChallengeTest.Fail(errorMessage);
            }
        }



        [Given(@"One time Challenge created successfully")]
        public void GivenOneTimeChallengeCreatedSuccessfully()
        {
            ParticipantListTest = extentTest.CreateNode("Participant List");
            if (FeatureContext.Current.ContainsKey("ChallengeCreated") && (bool)FeatureContext.Current["ChallengeCreated"])
            {
                Assert.IsTrue(true);
            }
            else
            {
                ParticipantListTest.Error("Challenge not created");
                Assert.Fail("Previous Test Case to create challenge failed");
            }
        }

        [Given(@"Administrator logged out of system")]
        public void Given_Administrator_Logged_Out_Of_System()
        {
            Thread.Sleep(1000);
            try
            {
                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        ParticipantListTest.Info("Administrator logged off successfully");
                    }
                    else
                    {
                        ParticipantListTest.Error("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    ParticipantListTest.Error("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Error("Failed to logoff administrator");
                Assert.Fail("Failed to logout administrator. Error: {0}", ex.Message);
                // do nothing
            }

        }

        [Given(@"User login to participant view using (.*),(.*) and click on login button")]
        public void Given_User_Logged_Into_Participant_View(string username, string password)
        {

            try
            {
                if (ParticipantListTest == null)
                {
                    extentTest = extent.CreateTest("PIV-1704");
                    ParticipantListTest = extentTest.CreateNode("Participant List");
                }
                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                loginPage = new LoginPage(driver);
                ParticipantListTest.Info("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(username);
                loginPage.PassTextBox.SendKeys(password);
                loginPage.LoginButton.Click();
                Thread.Sleep(1000);
                IWebElement logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    ParticipantListTest.Info(string.Format("Participant {0} Logged in successfully", username));

                }
                else
                {
                    ParticipantListTest.Fail("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Fail("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view.Error {0}", ex.ToString());
            }



        }

        [Given(@"Click on Challenges Tab")]
        public void Given_Click_On_Challenges_Tab()
        {
            try
            {
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    ParticipantListTest.Info("Challenge Tab Clicked");
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Fail("Failed to click on challenge tab");
                Assert.Fail(string.Format("Failed to load Challenge Tab. Error {0}", ex.Message));

            }

        }

        [Then(@"Challenge List should be visible")]
        public void Then_Challenge_List_Should_Be_Visible()
        {

            try
            {
                Thread.Sleep(1000);
                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    ParticipantListTest.Info(string.Format("{0} Challenges available for participant", challengeList.Count));
                }
                else
                {
                    ParticipantListTest.Error("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Error("Failed to load challenges");
                Assert.Fail("Failed to load any challenge. Error: {0}", ex.ToString());
            }
        }


        [When(@"(.*) found and clicked to see more details")]
        public void When_ChallengeName_Found_And_Clicked(string challengename)
        {
            try
            {
                string randomNumber = FeatureContext.Current != null && FeatureContext.Current.ContainsKey("ChallengeRandomNumber") ? FeatureContext.Current["ChallengeRandomNumber"].ToString() : string.Empty;
                challengename = challengename.Replace("{currentdate}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                challengename = challengename.Replace("{startofmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString());
                challengename = challengename.Replace("{startofnextmonth}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).ToShortDateString());

                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", challengename))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    ParticipantListTest.Info(string.Format("{0} available challenge found", challengename));
                }
                else
                {
                    ParticipantListTest.Error(string.Format("{0} available challenge not found", challengename));
                    Assert.Fail(string.Format("{0} available challenge not found", challengename));
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Error("Failed to load available challenges");
                Assert.Fail("Failed to load available challenge. Error: {0}", ex.Message);
            }

        }

        [Then(@"Participants tab should be visible")]
        public void Participant_Tab_Should_Be_Visible()
        {
            try
            {

                IWebElement participantTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Participants')]"));
                if (participantTabElement != null && participantTabElement.Displayed)
                {
                    ParticipantListTest.Pass("Participant tab is visible");
                    Assert.IsTrue(participantTabElement.Displayed);
                }
                else
                {
                    ParticipantListTest.Fail("Cannot find participants tab");
                    Assert.Fail("Cannot find participants tab");
                }

            }
            catch (Exception ex)
            {
                ParticipantListTest.Fail("Cannot find Participant tab");
                Assert.Fail("Cannot find Participant tab. Error: {0}", ex.ToString());

            }
        }

        [When(@"Participant joins the challenge")]
        public void When_Participant_Joins_The_Challenge()
        {

            try
            {
                IWebElement detailsTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Details')]"));
                if (detailsTabElement != null && detailsTabElement.Displayed)
                {
                    detailsTabElement.Click();
                    Thread.Sleep(1000);
                    try
                    {
                        IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
                        if (joinButtonElement != null && joinButtonElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            By loadingImage = By.TagName("http-busy");
                            GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                            joinButtonElement.Click();
                            ParticipantListTest.Info("Join Button available and clicked to join this challenge");
                        }
                        else
                        {
                            ParticipantListTest.Info("Join button not available or already joined");
                        }
                    }
                    catch
                    {
                        IWebElement joinedTextElement = driver.FindElement(By.ClassName("challengeJoinedText"));
                        if(joinedTextElement != null && joinedTextElement.Displayed)
                        {
                            ParticipantListTest.Info("Participant already joined");
                        }
                    }
                    
                }
                else
                {
                    ParticipantListTest.Fail("Cannot find details tab");
                    Assert.Fail("Cannot find details tab");
                }
            }
            catch (Exception ex)
            {
                ParticipantListTest.Error("Cannot find Join button for this challenge");
                Assert.Fail("Failed to load Join Button. Error: {0}", ex.ToString());
            }
        }


        [Then(@"Participant Table should contain name of (.*)")]
        public void Then_Participant_Table_Should_Contain_Name_Of_Challenger(string challengername)
        {
            try
            {
                IWebElement participantTabElement = driver.FindElement(By.XPath("//ul[contains(@class,'nav-tabs')]/li/a[contains(.,'Participants')]"));
                if (participantTabElement != null && participantTabElement.Displayed)
                {
                    participantTabElement.Click();
                    var challengerItemElement = driver.FindElement(By.XPath(string.Format("//div[@class='user-detail']/span[contains(.,'{0}')]", challengername)));
                    if (challengerItemElement != null && challengerItemElement.Displayed)
                    {
                        ParticipantListTest.Pass(string.Format("{0} Challenger available in list", challengername));
                        Assert.IsTrue(challengerItemElement.Displayed);
                    }
                    else
                    {
                        ParticipantListTest.Fail(string.Format("{0} Challenger NOT available in list", challengername));
                        Assert.Fail("Failed to retrieve participant {0}", challengername);
                    }
                }
                else
                {
                    ParticipantListTest.Fail("Cannot find participants tab");
                    Assert.Fail("Cannot find participants tab");
                }
              
            }
            catch (Exception ex)
            {
                ParticipantListTest.Fail("Participant not available");
                Assert.Fail("Failed to retrieve participant {0}. Error: {1}", challengername, ex.ToString());
            }
          
        }





        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }

        private DateTime GetStartOfWeek(DateTime input)
        {
            // Using +6 here leaves Monday as 0, Tuesday as 1 etc.
            int dayOfWeek = (((int)input.DayOfWeek) + 6) % 7;
            return input.Date.AddDays(-dayOfWeek);
        }

        private int GetWeeks(DateTime start, DateTime end)
        {
            start = GetStartOfWeek(start);
            end = GetStartOfWeek(end);
            int days = (int)(end - start).TotalDays;
            return (days / 7) + 1; // Adding 1 to be inclusive
        }


    }
}
