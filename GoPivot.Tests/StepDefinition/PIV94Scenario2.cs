﻿using GoPivot.Tests.BaseClasses;
using GoPivot.Tests.ComponentHelper;
using GoPivot.Tests.PageObject;
using GoPivot.Tests.Settings;
using NUnit;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Collections.Generic;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports;

namespace GoPivot.Tests.StepDefinition
{
    [Binding, Scope(Feature = "PIV-94 Scenario 2")]
    public class PIV94Scenario2 : BaseStepDefinition
    {
        private LoginPage loginPage;

        public PIV94Scenario2()
        {

        }
        private ExtentTest CreateChallengeTest;
        private ExtentTest JoinChallengeTest;
        private ExtentTest DailyGoalChallengeTest;
        [Given(@"I login to admin view (.*), (.*) and click on login button")]
        public void GivenILoginToAdminViewAndClickOnLoginButton(string username, string password)
        {
            extentTest = extent.CreateTest("PIV-94 Scenario 2");
            CreateChallengeTest = extentTest.CreateNode("Create Jump Start Challenge");
            loginPage = new LoginPage(driver);
            CreateChallengeTest.Info("Login Page loaded successfully");
            loginPage.LoginTextBox.SendKeys(username);
            loginPage.PassTextBox.SendKeys(password);
            loginPage.LoginButton.Click();
            CreateChallengeTest.Info(string.Format("Administrator {0} Logged in successfully", username));
        }

        [When(@"Selects (.*) from the list")]
        public void WhenSelectsFromTheList(string client)
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            var clientElement = adminPage.GetClient(client);
            if (clientElement != null && clientElement.Displayed)
            {
                CreateChallengeTest.Info(string.Format("Client {0} selected", client));
                clientElement.Click();
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Client {0} not found", client));
                Assert.Fail(string.Format("Client {0} not found", client));
            }
        }

        [When(@"Clicks (.*) from Program List")]
        public void WhenClicksFromProgramList(string program)
        {
            var adminPage = new AdminPage(driver);
            var programElement = adminPage.GetProgram(program);
            if (programElement != null && programElement.Displayed)
            {
                programElement.Click();
                CreateChallengeTest.Info(string.Format("Program {0} selected", program));
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Program {0} not found", program));
            }
        }

        [When(@"Admin Selects Challenges Tab")]
        public void WhenAdminSelectsChallengesTab()
        {
            Thread.Sleep(1000);
            var adminPage = new AdminPage(driver);
            var challengeTab = adminPage.GetProgramTab("Challenges");
            if (challengeTab == null)
            {
                CreateChallengeTest.Error("Unable to find challenges tab");
                Assert.Fail("Unable to find challenges tab");
            }
            else
            {
                CreateChallengeTest.Info("Challenge Tab loaded");
                challengeTab.Click();
            }

        }

        [When(@"Clicks on New Challenge")]
        public void WhenClicksOnNewChallenge()
        {
            Thread.Sleep(5000);
            AdminPage adminPage = new AdminPage(driver);
            adminPage.GetAddChallengeButton();
            if (adminPage.AddChallengeButton != null)
            {
                adminPage.AddChallengeButton.Click();
                CreateChallengeTest.Info("Add Challenge Button clicked");
                Thread.Sleep(2000);
            }
            else
            {
                CreateChallengeTest.Error("Cannot add new challenge");
                Assert.Fail("cannot add new challenge");
            }
        }

        [When(@"Clicks on Wellness Type of Challenge")]
        public void WhenClicksOnWellnessTypeOfChallenge()
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeCategory = adminPage.GetChallengeCategory("Wellness");
            if (challengeCategory != null)
            {
                challengeCategory.Click();
                CreateChallengeTest.Info("Wellness Challenge type selected");
            }
            else
            {
                CreateChallengeTest.Error("Wellness Challenge Type not available");
                Assert.Fail("Wellness Challenge Type not available");
            }

        }

        [When(@"Selects (.*) from the challenge type list")]
        public void WhenSelectsFromTheChallengeTypeList(string wellnesschallengetype)
        {
            Thread.Sleep(2000);
            AdminPage adminPage = new AdminPage(driver);
            var challengeType = adminPage.GetChallengeType(wellnesschallengetype);
            if (challengeType != null)
            {
                challengeType.Click();
                CreateChallengeTest.Info(string.Format("{0} Challenge Type selected", wellnesschallengetype));
            }
            else
            {
                string errorMessage = string.Format("{0} Challenge type not found", wellnesschallengetype);
                CreateChallengeTest.Error(errorMessage);
                Assert.Fail(errorMessage);
            }

        }


        [When(@"i fill (.*), (.*), (.*) and (.*)")]
        public void WhenIFillAnd(string challengename, string startdate, string enddate, string description)
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            string randomNumber = new Random().Next(1000).ToString();
            challengename = challengename.Replace("{currentdate}", DateTime.Now.ToShortDateString());
            challengename = challengename.Replace("{randomnumber}", randomNumber);
            FeatureContext.Current.Add("ChallengeRandomNumber", randomNumber);
            startdate = startdate.Replace("{currentdate}", DateTime.Now.ToShortDateString());
            bool success = addNewChallengePage.FillStepForJumpStart(challengename, startdate, enddate, description, out errorMessage);
            if (success)
            {
                Thread.Sleep(2000);
                CreateChallengeTest.Info("Add Challenge information submitted {{Step 1}}");
                addNewChallengePage.Step1_NextButton.Click();
            }
            else
            {
                extent.Equals(errorMessage);
                Assert.Fail(errorMessage);
            }
        }


        [When(@"Searches for Participant (.*) and add into right section")]
        public void WhenSearchesForParticipantAndAddIntoRightSection(string challengername)
        {
            AddNewChallengePage addChallengerPage = new AddNewChallengePage(driver);
            string errorMessage = string.Empty;
            bool success = addChallengerPage.AddChallengers(challengername, out errorMessage);
            if (success)
            {
                CreateChallengeTest.Info(string.Format("Participant {0} added to challenge", challengername));
            }
            else
            {
                CreateChallengeTest.Error(string.Format("Failed to add {0} challenger. Error: {1}", challengername, errorMessage));
                Assert.Fail(errorMessage);
            }

        }

        [When(@"User tries to save new challenge")]
        public void WhenUserTriesToSaveNewChallenge()
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            addNewChallengePage.Step2_CreateButton.Click();
            CreateChallengeTest.Info("Create Challenge button clicked");
        }

        [Then(@"Client link should be visible")]
        public void ThenClientLinkShouldBeVisible()
        {
            Thread.Sleep(3000);
            var adminPage = new AdminPage(driver);
            if (adminPage.Clients != null && adminPage.Clients.Displayed)
            {
                adminPage.Clients.Click();
                CreateChallengeTest.Info("Clients link clicked");
            }
            else
            {
                CreateChallengeTest.Fail("Clients Link not found");
                Assert.Fail("Clients link not found");
            }
        }

        [Then(@"Client details page will be loaded")]
        public void ThenClientDetailsPageWillBeLoaded()
        {

            var adminPage = new AdminPage(driver);
            if (!adminPage.EditClient.Displayed)
            {
                CreateChallengeTest.Fail("Clients Details not found");
                Assert.Fail("Clients Details not found");

            }
            else
            {
                CreateChallengeTest.Info("Client Details page loaded");
            }
        }

        [Then(@"Program tabs will be listed")]
        public void ThenProgramTabsWillBeListed()
        {
            var adminPage = new AdminPage(driver);
            if (adminPage.ProgramTabs.Count > 0)
            {
                CreateChallengeTest.Info("Program Listing page loaded");

            }
            else
            {
                CreateChallengeTest.Error("Program details page not loaded");
                Assert.Fail("Failed to load Program details");
            }
        }

        [Then(@"Participants should be listed")]
        public void ThenParticipantsShouldBeListed()
        {
            AddNewChallengePage addNewChallengePage = new AddNewChallengePage(driver);
            var participantCount = addNewChallengePage.Step2_ParticipantList;
            if (participantCount != null && participantCount.Count > 0)
            {
                CreateChallengeTest.Info("Participants available to select");
            }
            else
            {
                CreateChallengeTest.Error("No Participants available");
                Assert.Fail("No Participants available");
            }
        }

        [Then(@"New challenge (.*) should be listed in challenge list")]
        public void ThenNewChallengeShouldBeListedInChallengeList(string challengename)
        {

            string randomNumber = FeatureContext.Current != null && FeatureContext.Current.ContainsKey("ChallengeRandomNumber") ? FeatureContext.Current["ChallengeRandomNumber"].ToString() : string.Empty;
            challengename = challengename.Replace("{currentdate}", DateTime.Now.ToShortDateString());
            challengename = challengename.Replace("{randomnumber}", randomNumber);
            WebDriverWait wait = GetWebDriverWait();
            var addedChallenge = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//table[@class='table table-hover adminTable']/tbody/tr[1]/td[3]")));
            if (addedChallenge != null && addedChallenge.Displayed && addedChallenge.Text.ToLower().Contains(challengename.ToLower()))
            {
                string message = string.Format("Challenge {0} added successfully", challengename);
                CreateChallengeTest.Pass(message);
                FeatureContext.Current["ChallengeCreated"] = true;
                Assert.IsTrue(true);
            }
            else
            {
                string errorMessage = string.Format("Cannot find {0} challenge in challenge list", challengename);
                Assert.Fail(errorMessage);
                CreateChallengeTest.Fail(errorMessage);
            }
        }

        [Given(@"User login to participant view using (.*),(.*) and click on login button")]
        public void Given_User_Logged_Into_Participant_View(string username, string password)
        {

            try
            {
                loginPage = new LoginPage(driver);
                JoinChallengeTest.Info("Login Page loaded successfully");
                loginPage.LoginTextBox.SendKeys(username);
                loginPage.PassTextBox.SendKeys(password);
                loginPage.LoginButton.Click();

                IWebElement logoutElement = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Log Out")));
                if (logoutElement != null && logoutElement.Displayed)
                {
                    JoinChallengeTest.Info(string.Format("Participant {0} Logged in successfully", username));

                }
                else
                {
                    JoinChallengeTest.Fail("Login Failed. Failed to load participant view");
                    Assert.Fail("Login Failed. Failed to load participant view");
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Fail("Login Failed. Failed to load participant view");
                Assert.Fail("Login Failed. Failed to load participant view.Error {0}", ex.ToString());
            }



        }

        [Given(@"Click on Challenges Tab")]
        public void Given_Click_On_Challenges_Tab()
        {
            try
            {
                IWebElement challengeTab = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Challenges")));
                if (challengeTab != null && challengeTab.Displayed)
                {
                    challengeTab.Click();
                    JoinChallengeTest.Info("Challenge Tab Clicked");
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Fail("Failed to click on challenge tab");
                Assert.Fail(string.Format("Failed to load Challenge Tab. Error {0}", ex.Message));

            }

        }

        [Then(@"Challenge List should be visible")]
        public void Then_Challenge_List_Should_Be_Visible()
        {

            try
            {
                var challengeList = driver.FindElements(By.XPath("//div[@class='panel challenge ng-scope']"));
                if (challengeList != null && challengeList.Count > 0)
                {
                    JoinChallengeTest.Info(string.Format("{0} Challenges available for participant", challengeList.Count));
                }
                else
                {
                    JoinChallengeTest.Error("Failed to load challenges");
                    Assert.Fail("Failed to load challenges");
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Error("Failed to load challenges");
                Assert.Fail("Failed to load any challenge. Error: {0}", ex.Message);
            }
        }

        [When(@"(.*) found from available section and clicked")]
        public void When_ChallengeName_Found_From_Available_Section_And_Clicked(string challengename)
        {
            try
            {
                string randomNumber = FeatureContext.Current != null && FeatureContext.Current.ContainsKey("ChallengeRandomNumber") ? FeatureContext.Current["ChallengeRandomNumber"].ToString() : string.Empty;
                challengename = challengename.Replace("{currentdate}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                var challenge = GetWebDriverWait().Until(ExpectedConditions.ElementIsVisible(By.XPath(string.Format("//span[@ng-if='!showAvailableChallenges']//div[@class='panel challenge ng-scope']/p[contains(text(),'{0}')]", challengename))));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    JoinChallengeTest.Info(string.Format("{0} available challenge found", challengename));
                }
                else
                {
                    JoinChallengeTest.Error(string.Format("{0} available challenge not found", challengename));
                    Assert.Fail(string.Format("{0} available challenge not found", challengename));
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Error("Failed to load available challenges");
                Assert.Fail("Failed to load available challenge. Error: {0}", ex.Message);
            }

        }





        [Given(@"Jump Start Your Day Challenge created successfully")]
        public void Given_Jump_Start_Your_Day_Challenge_Created_Successfully()
        {
            JoinChallengeTest = extentTest.CreateNode("Join the Challenge");
            if (FeatureContext.Current.ContainsKey("ChallengeCreated") && (bool)FeatureContext.Current["ChallengeCreated"])
            {
                Assert.IsTrue(true);
            }
            else
            {
                JoinChallengeTest.Error("Challenge not created");
                Assert.Fail("Previous Test Case to create challenge failed");
            }
        }

        [Given(@"Administrator logged out of system")]
        public void Given_Administrator_Logged_Out_Of_System()
        {
            Thread.Sleep(1000);
            try
            {
                var dropdownMenuElement = driver.FindElement(By.XPath("//a[contains(.,'Log Out')]/../../preceding-sibling::a"));
                if (dropdownMenuElement.Displayed)
                {
                    dropdownMenuElement.Click();

                    var logoutElement = driver.FindElement(By.LinkText("Log Out"));
                    if (logoutElement != null && logoutElement.Displayed)
                    {
                        logoutElement.Click();
                        JoinChallengeTest.Info("Administrator logged off successfully");
                    }
                    else
                    {
                        JoinChallengeTest.Error("Logout link not clickable");
                        Assert.Fail("Logout link not clickable");
                    }

                }
                else
                {
                    JoinChallengeTest.Error("Logout Dropdown not visible");
                    Assert.Fail("Logout Dropdown not visible");
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Error("Failed to logoff administrator");
                Assert.Fail("Failed to logout administrator. Error: {0}", ex.Message);
                // do nothing
            }

        }

        [Then(@"Join button should be visible and click to join the challenge")]
        public void Then_Join_Button_Should_Be_Visible_And_Click_To_Join_The_Challenge()
        {
         
            try
            {
                IWebElement joinButtonElement = GetWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(.,'Join!')]")));
                if (joinButtonElement != null && joinButtonElement.Displayed)
                {
                    Thread.Sleep(1000);
                    By loadingImage = By.TagName("http-busy");
                    GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

                    joinButtonElement.Click();
                    JoinChallengeTest.Info("Join Button available and clicked to join this challenge");
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Error("Cannot find Join button for this challenge");
                Assert.Fail("Failed to load Join Button. Error: {0}", ex.ToString());
            }
        }

        [When(@"(.*) listed in joined list")]
        public void When_Challenge_Listed_In_Joined_List(string challengename)
        {
            Thread.Sleep(1000);
            try
            {
                By loadingImage = By.TagName("http-busy");
                GetWebDriverWait().Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));
                string randomNumber = FeatureContext.Current != null && FeatureContext.Current.ContainsKey("ChallengeRandomNumber") ? FeatureContext.Current["ChallengeRandomNumber"].ToString() : string.Empty;
                challengename = challengename.Replace("{currentdate}", DateTime.Now.ToShortDateString());
                challengename = challengename.Replace("{randomnumber}", randomNumber);
                var challenge = driver.FindElement(By.XPath(string.Format("//span[@ng-if='!showJoinedChallenges']//div[@class='panel challenge ng-scope']/p[contains(.,'{0}')]", challengename)));
                if (challenge != null && challenge.Displayed)
                {
                    challenge.Click();
                    JoinChallengeTest.Info(string.Format("{0} joined challenge found", challengename));
                }
                else
                {
                    JoinChallengeTest.Error(string.Format("{0} joined challenge not found", challengename));
                    Assert.Fail(string.Format("{0} joined challenge not found", challengename));
                }
            }
            catch (Exception ex)
            {
                JoinChallengeTest.Error("Failed to load joined challenges");
                Assert.Fail("Failed to load joined challenge. Error: {0}", ex.ToString());
            }
        }

        [Then(@"Calendars should be visible between (.*) and (.*)")]
        public void Then_Calendars_Should_Be_Visible(string challengestartdate, string challengeenddate)
        {
            try
            {
                DailyGoalChallengeTest = extentTest.CreateNode("Daily Goals");
                challengestartdate = challengestartdate.Replace("{currentdate}", DateTime.Now.ToShortDateString());
                DateTime challengeStartDate = DateTime.Now;
                DateTime challengeEndDate = DateTime.Now;

                DateTime.TryParse(challengestartdate, out challengeStartDate);
                DateTime.TryParse(challengeenddate, out challengeEndDate);

                int numberOfMonths = Math.Abs(((challengeStartDate.Year - challengeEndDate.Year) * 12) + challengeStartDate.Month - challengeEndDate.Month) + 1;

                IList<IWebElement> calendarContainers = driver.FindElements(By.XPath("//div[@class = 'calendarMonth ng-binding']"));
                if (calendarContainers != null && calendarContainers.Count > 0 && calendarContainers.Count == numberOfMonths)
                {
                    DailyGoalChallengeTest.Info(string.Format("{0} Calendar(s) found", calendarContainers.Count));

                }
                else
                {
                    DailyGoalChallengeTest.Error("No Calendars available");
                    Assert.Fail("No Calendars available");
                }
            }
            catch (Exception ex)
            {
                DailyGoalChallengeTest.Error("Failed to load calendar details");
                Assert.Fail("Failed to load calendar details", ex.ToString());
            }


        }

        [When(@"I select current day from calendar")]
        public void When_I_Select_Current_Day_From_Calendar()
        {
            try
            {
                string month = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);

                IList<IWebElement> calendarDays = driver.FindElements(By.XPath(string.Format("//div[@class = 'calendarMonth ng-binding'  and contains(.,'{0}')]/../div[contains(@class,'calendarDays')]", month)));
                if (calendarDays != null && calendarDays.Count > 0)
                {
                    var dateElement = calendarDays.FirstOrDefault(p => p.Text == DateTime.Now.Day.ToString());
                    if (dateElement != null)
                    {
                        var iconCheck = dateElement.FindElement(By.ClassName("icon-check"));
                        dateElement.Click();
                        DailyGoalChallengeTest.Info(string.Format("Current Date {0} selected to log goal for the day", DateTime.Now.ToShortDateString()));
                    }
                    else
                    {
                        DailyGoalChallengeTest.Info(string.Format("Goal already logged for current date {0}", DateTime.Now.ToShortDateString()));
                    }

                }
                else
                {
                    DailyGoalChallengeTest.Error("No Calendar found to select goal for current date");
                    Assert.Fail("No Calendar found to select goal for current date");
                }
            }
            catch (Exception ex)
            {
                DailyGoalChallengeTest.Error("No Calendar found to select goal for current date");
                Assert.Fail(string.Format("No Calendar found to select goal for current date. Error: {0}", ex.ToString()));

            }
        }

        [Then(@"Goal Count should be greater than one")]
        public void ThenGoalCountShouldBeEqualsToCount()
        {
            int goalCount = 0;
            try
            {
                var goalCountElement = driver.FindElement(By.XPath("//div[contains(@class,'goalMetNumber')]"));
                string goalCountText = goalCountElement.Text;

                Int32.TryParse(goalCountText, out goalCount);
                Assert.True(goalCount > 0);
                DailyGoalChallengeTest.Pass(string.Format("Goal Count is greater than 0. Actual Count: {0}", goalCount));

            }
            catch (Exception ex)
            {
                DailyGoalChallengeTest.Error("Goals not matching");
                Assert.Fail("Goal count not matching. Error: {0}", ex.ToString());

            }
        }




        private WebDriverWait GetWebDriverWait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60))
            {
                PollingInterval = TimeSpan.FromMilliseconds(250)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait;
        }
    }
}
