﻿Feature: PIV-1704
	As a participant, 
	I want to see a list of individual users who have joined a challenge before it starts 
	so I know who else is participating in it

Scenario Outline: Create new Custom One time Challenge having future date to start
	Given I login to admin view <username>, <password> and click on login button
	Then Client link should be visible
	When Selects <client> from the list
	Then Client details page will be loaded
	When Clicks <program> from Program List
	Then Program tabs will be listed
	When Admin Selects Challenges Tab
	And Clicks on New Challenge
	And Clicks on Custom Type of Challenge
	And Selects first option from challenge types
	And i fill <challengename>,<startdate>,<enddate>,<customchallengeactiontext>,<customchallengegoal>,<customchallengeunit>,<customchallengetime> and <goaltext>
	Then Participants should be listed
	When Every participants added and saves the challenge
	Then New challenge <challengename> should be listed in challenge list

	Examples: 
	| username  | password  | client                  | program         | challengename                                   | startdate          | enddate          | customchallengeactiontext | customchallengegoal | customchallengeunit | customchallengetime | goaltext         |
	| dinesh.gm | Tp123456! | ACME Health and Fitness | Human Resources | Future Custom {startofnextmonth}-{randomnumber} | {startofnextmonth} | {endofnextmonth} | Future One time challenge | 50                  | time(s)             | one time            | One time 50 only |

Scenario Outline: Participants Tab
	Given One time Challenge created successfully
	And Administrator logged out of system
	Given User login to participant view using <username>, <password> and click on login button
	And Click on Challenges Tab
	Then Challenge List should be visible
	When <challengename> found and clicked to see more details
	Then Participants tab should be visible
	When Participant joins the challenge
	Then Participant Table should contain name of <challengername>
Examples:
	| username | password  | challengename               | challengername |
	| bbanner6 | P@ssw0rd1 | Future Custom 11/1/2018-858 | Kelli Ballard  |
