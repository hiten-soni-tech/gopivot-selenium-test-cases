﻿Feature: PIV-94 Scenario 5
	 Display progress section for One-time Completion challenges

@PIV-94-Scenario-5-Create-Custom-One-Time-Challenge
Scenario Outline: Create new Custom One time Challenge  
	Given I login to admin view <username>, <password> and click on login button
	Then Client link should be visible
	When Selects <client> from the list
	Then Client details page will be loaded
	When Clicks <program> from Program List
	Then Program tabs will be listed
	When Admin Selects Challenges Tab
	And Clicks on New Challenge
	And Clicks on Custom Type of Challenge
	And Selects first option from challenge types
	And i fill <challengename>,<startdate>,<enddate>,<customchallengeactiontext>,<customchallengegoal>,<customchallengeunit>,<customchallengetime> and <goaltext>
	Then Participants should be listed
	When Searches for Participant <challengername> and add into right section
	And User tries to save new challenge
	Then New challenge <challengename> should be listed in challenge list

	Examples: 
	| username  | password  | client                  | program         | challengename                                | startdate      | enddate    | customchallengeactiontext | customchallengegoal | customchallengeunit | customchallengetime | goaltext         | challengername |
	| dinesh.gm | Tp123456! | ACME Health and Fitness | Human Resources | Custom Onetime {startofmonth}-{randomnumber} | {startofmonth} | 02/22/2019 | One time challenge        | 50                  | time(s)             | one time             | One time 50 only | Kelli Ballard  |

@PIV-94-Scenario-5-Join-The-Challenge
Scenario Outline: Join the Challenge
	Given One time Challenge created successfully
	And Administrator logged out of system
	Given User login to participant view using <username>, <password> and click on login button
	And Click on Challenges Tab
	Then Challenge List should be visible
	When <challengename> found from available section and clicked
	Then Goal Description should be <goaltext>
	And Join button should be visible and click to join the challenge
	And Goal Text should be No Goals Met
	When Goal logged on <challengestartdate> having time <customchallengegoal>
	Then Goals met text should be changed to Yes

	Examples:
	| username | password  | challengename                                | challengestartdate | challengeenddate | goaltext         | customchallengegoal |
	| bbanner6 | P@ssw0rd1 | Custom Onetime {startofmonth}-{randomnumber} | {startofmonth}     | 02/22/2019       | One time 50 only | 50                  |
