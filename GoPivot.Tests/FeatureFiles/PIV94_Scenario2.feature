﻿Feature: PIV-94 Scenario 2
	 Display progress section for Daily Completion challenges

#@PIV-94-Scenario-2-Create-Jump-Start-Challenge
Scenario Outline: Create Jump start your day Challenge 
	Given I login to admin view <username>, <password> and click on login button
	Then Client link should be visible
	When Selects <client> from the list
	Then Client details page will be loaded
	When Clicks <program> from Program List
	Then Program tabs will be listed
	When Admin Selects Challenges Tab
	And Clicks on New Challenge
	And Clicks on Wellness Type of Challenge
	And Selects <wellnesschallengetype> from the challenge type list
	And i fill <challengename>, <startdate>, <enddate> and <description>
	Then Participants should be listed
	When Searches for Participant <challengername> and add into right section
	And User tries to save new challenge
	Then New challenge <challengename> should be listed in challenge list

	Examples: 
	| username  | password  | client                  | program         | wellnesschallengetype | challengename                  | startdate  | enddate    | description           | challengername |
	| dinesh.gm | Tp123456! | ACME Health and Fitness | Human Resources | Jump Start Your Day   | Jump Start Your Day {currentdate}-{randomnumber}| {currentdate}| 02/22/2019 | Eat Fruits every day | Kelli Ballard  |


@PIV-94-Scenario-2-Join-The-Challenge-And-Add-Goals
Scenario Outline: Join the Challenge
	Given Jump Start Your Day Challenge created successfully
	And Administrator logged out of system
	Given User login to participant view using <username>, <password> and click on login button
	And Click on Challenges Tab
	Then Challenge List should be visible
	When <challengename> found from available section and clicked
	Then Join button should be visible and click to join the challenge
	#When <challengename> listed in joined list
	Then Calendars should be visible between <challengestartdate> and <challengeenddate>	
	When I select current day from calendar
	Then Goal Count should be greater than one
	Examples:
	| username | password  | challengename                                   | challengestartdate | challengeenddate |
	| bbanner6 | P@ssw0rd1 | Jump Start Your Day {currentdate}-{randomnumber}| {currentdate}        | 02/22/2019       |