﻿Feature: PIV-94 Scenario 1
		 View Challenge Progress Tab (Post-join, Post-start) - Completion Metrics

@PIV-94-Scenario-1-Create-Healthy-Weight-Loss-Challenge
Scenario Outline: Create Healthy Weight Loss Challenge 
	Given I login to admin view <username>, <password> and click on login button
	Then Client link should be visible
	When Selects <client> from the list
	Then Client details page will be loaded
	When Clicks <program> from Program List
	Then Program tabs will be listed
	When Admin Selects Challenges Tab
	And Clicks on New Challenge
	And Clicks on Wellness Type of Challenge
	And Selects <wellnesschallengetype> from the challenge type list
	And i fill <challengename>, <startdate>, <enddate>, <goalvalue>, <timeline> and <description>
	Then Participants should be listed
	When Searches for Participant <challengername> and add into right section
	And User tries to save new challenge
	Then New challenge <challengename> should be listed in challenge list

	Examples: 
	| username  | password  | client                  | program         | wellnesschallengetype | challengename                            | startdate  | enddate    | goalvalue | timeline  | description          | challengername |
	| dinesh.gm | Tp123456! | ACME Health and Fitness | Human Resources | Healthy Weight Loss   | Healthy Weight Loss Challenge 10/20/2018 | 10/20/2018 | 02/22/2019 | 20        | each week | Every Week 20 pounds | Kelli Ballard  |

@PIV-94-Scenario-1-Display-Goal
Scenario Outline: Display Goal into Participant Account
	Given Healthy Weight Loss Challenge created successfully
	And Administrator logged out of system
	And User login to participant view using <username>, <password> and click on login button
	And Click on Challenges Tab
	Then Challenge List should be visible
	When <challengename> found from available section and clicked
	Then Goal <goaldescription> should be equal to challenge created 

	Examples:
	| username | password  | challengename                            | goaldescription      |
	| bbanner6 | P@ssw0rd1 | Healthy Weight Loss Challenge 10/20/2018 | Every Week 20 pounds |
